//Câu lệnh này tương tự import express from 'express'; Dùng để import thư viện vào project
const express = require("express");

//Khởi tạo app express
const app = express();

//Khai báo cổng của project 
const port = 8000;

app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})

//Khai báo sử dụng các tài nguyên static (images, js, css, v...v...)
app.use(express.static("views"));

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/course365", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/course365.html'));
})

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + port);
})
